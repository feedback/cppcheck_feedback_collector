import os
from pathlib import Path
import json
import xmltodict
import copy
import sys

cwdStr = " "

def func():
    currentDirectory = os.getcwd()
    cppCheckStr = "cppcheck " + currentDirectory + " --xml 2> testResults.xml"
    os.system(cppCheckStr)

def checkIfFileEmpty():
    check = -1
    
    if os.stat("testResults.xml").st_size == 0:
        check = 0
    else:
        check = 1
    
    return check

def xmlFunc():
    with open("testResults.xml") as xml_file:
        data = xmltodict.parse(xml_file.read())
        xml_file.close()

        jsonData = json.dumps(data, indent=3)

        with open("testResults.json", "w") as json_file:
            json_file.write(jsonData)
            json_file.close()

def editJsonData():
    with open("testResults.json", "r+") as json_file:
        data = json.load(json_file)
        data2 = data['results']['errors']['error']

        if not '@id' in data2:
            #Explanation: Originally used for loops, but as we delete data, the range 
            #changes, and we need this to happen dynamically. The only way to do this is to use
            #a while loop and variables
            range1 = len(data2)
            z = 0
            j = -1
            while z < range1:
                while j < range1:
                    if 'name' in data2[j]:
                        if data2[z]['@id'] == data2[j]['name']: #if error already exists in json
                            #found error, will increment count and break
                            data2[j]['count'] += 1

                            #removing full path from current directory to give local view
                            tempStr = data2[z]['location']['@file']
                            tempStr1 = tempStr.replace(cwdStr, "")

                            #Grabbing line num to append to tempStr1
                            lineNum = data2[z]['location']['@line']
                            tempStr1 += " Line: " + lineNum
                            data2[j]['moderateLocations'].append(tempStr1)

                            #removing data2 as it is a repeat error
                            del data2[z]

                            #Since all data2 indexes are now being bumped back, we need
                            #to decrement back the len(data2) = range, as well as the z
                            #value used to index, being "z" and "j"
                            range1 -= 1
                            z -= 1
                            j -= 1
                            break
                    
                    #as we need j to act like a for loop, increments every iteration
                    j += 1
                else:
                    #error not found, so adding new json field
                    data2[z]['name'] = data2[z].pop('@id')
                    data2[z]['reasoning'] = data2[z].pop('@msg')
                    data2[z]['count'] = 1
                    data2[z].pop('@verbose')
                    data2[z].pop('@severity')
                    data2[z].pop('@cwe')
                    
                    if 'symbol' in data2[z]:
                        data2[z].pop("symbol")
                    
                    #Removing full path from file view to give local view
                    tempStr = data2[z]['location']['@file']
                    tempStr1 = tempStr.replace(cwdStr, "")
                    lineNum = data2[z]['location']['@line']
                    data2[z].pop('location')

                    #attempt to make 'location' tag, comment out if not working
                    #creating list, then adding the line
                    data2[z]['moderateLocations'] = []
                    tempStr1 += " Line: " + lineNum
                    data2[z]['moderateLocations'].append(tempStr1)
                    #z += 1
                
                #since j acts as for loop, resets here. likewise, z increments
                z += 1
                j = 0
                    
                
            
        data3 = {}
        data3['title'] = 'CPP Check'
        data3['feedbackItems'] = data2

        #creating a deep copy, as overview looks different than feedback
        #EASIER THAN CREATING CUSTOM JSON
        overviewData = copy.deepcopy(data2)

        if not '@id' in overviewData:
            for i in range(len(overviewData)):
                overviewData[i].pop('reasoning')
                overviewData[i].pop('moderateLocations')
                data2[i].pop('count')

        else: #CASE OF ONLY 1 FEEDBACK
            #altering overview and feedbackData for case of 1 file
            overviewData.pop('@severity')
            overviewData.pop('@verbose')
            overviewData.pop('@cwe')
            overviewData.pop('location')
            overviewData.pop('@msg')
            
            if 'symbol' in overviewData:
                overviewData.pop('symbol')
            overviewData['name'] = overviewData.pop('@id')
            overviewData['count'] = 1

            #altering data2 aka feedbackData accordingly
            data2['name'] = data2.pop('@id')
            data2['reasoning'] = data2.pop('@msg')
            data2.pop('@severity')
            data2.pop('@verbose')
            data2.pop('@cwe')

            if 'symbol' in data2:
                data2.pop('symbol')
            
            #Parsing the location and changing it to specs
            tempStr = data2['location']['@file']
            lineNum = data2['location']['@line']
            data2.pop('location')

            data2['moderateLocations'] = []
            tempStr1 = tempStr.replace(cwdStr, "")
            tempStr1 += " Line: " + lineNum
            data2['moderateLocations'].append(tempStr1)


        data3['overview'] = overviewData
        
        jsonData = json.dumps(data3, indent=3)
        with open("testResults.json", "w") as json_file:
            json_file.write(jsonData)
            json_file.close()

        
def removeXmlFile():
    os.remove("testResults.xml")
        
def noErrorCheck():
    with open("testResults.json", "r+") as json_file:
        data = json.load(json_file)

        if data['results']['errors'] == None:
            return 0
        else:
            return 1


def noErrorJsonUpdate():
    with open("testResults.json", "r+") as json_file:
        data = json.load(json_file)
        data.pop('results')
        data['title'] = 'cppCheck'
        data['feedbackItems'] = []
        feedbackData = {"name" : "Passed",
                        "reasoning" : "All tests passed; No errors found!",
                        "moderateLocations" : ['null']
                        }
        data['feedbackItems'].append(feedbackData)
        data['overview'] = []
        overviewData = {
            "name" : "passed",
            "count": 1
        }
        data['overview'].append(overviewData)
        print(data)

        jsonData = json.dumps(data, indent=3)
        with open("testResults.json", "w") as json_file1:
            json_file1.write(jsonData)
            json_file1.close()


def moveFile():
    if (len(sys.argv)) == 1:
        print("no path, exiting!")
    elif len(sys.argv) == 2:
        newDir = cwdStr + sys.argv[1] + "/" + "testResults.json"
        curFile = cwdStr + "testResults.json"
        os.replace(curFile, newDir)
        

if __name__ == "__main__":
   cwdStr = str(Path.cwd()) + "/"
   func()
   check = checkIfFileEmpty()
   if check == 1: #if an error xml file exists
       xmlFunc()
       errorCheck = noErrorCheck() #checking if there are any errors inside of the xml file, will break parsing without check 
       if (errorCheck == 1):
           editJsonData()
       else:
           noErrorJsonUpdate()
       
       moveFile()

   removeXmlFile()
    
# if __name__ == "__main__":
#     cwdStr = str(Path.cwd()) + "/"
    
#     jsonData = {
#             "title": "Java Code Smells",
#             "feedbackItems": [
#             {
#                 "name": "Too Many Parameters"
#             }
#         ],
#             "overview": [
#             {
#                 "name": "Too Many Parameters",
#                 "count": 1
#             }
#         ]
#     }

#     #print(jsonData)

#     jsonData1 = json.dumps(jsonData, indent=3)

#     #print(jsonData1)

#     with open("testResults.json", "w") as json_file:
#         json_file.write(jsonData1)
#         json_file.close()

#     if len(sys.argv) == 2:
#         newDir = cwdStr + sys.argv[1] + "/testResults.json"
#         curFile = cwdStr + "testResults.json"
#         os.replace(curFile, newDir)