# cppcheck_feedback_collector

gitlab runner script for collecting feedback from cppcheck

## Running
- put the runner in the directory is should be running in
- type "python3 cppcheckRunner.py Location"

"Location" is an optional flag of cppcheckRunner.py, where Location = a folder that exists in the root directory. (IE "python3 cppcheckRunner.py exampleDirectory") If the Location flag is left blank, the testResult.json file will be placed in the root directory.

## Dependencies required
- xmltodict
- python3
- pip

## Tests:

- Empty directory: Passed
- directory with 1 file: Passed
- 2 level directory: Passed
- Angel A2 directory: Passed
- Angel A3 w/ JS directory: Passed (Works on A4 Dir as well) [Ben's 2750 A4 Repo] (https://github.com/BCarlson1512/The-GPX-Project)
